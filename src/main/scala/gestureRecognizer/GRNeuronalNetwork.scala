package gestureRecognizer

import java.util

import org.deeplearning4j.nn.conf.layers.{DenseLayer, OutputLayer}
import org.deeplearning4j.nn.conf.{MultiLayerConfiguration, NeuralNetConfiguration}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize
import org.nd4j.linalg.lossfunctions.LossFunctions

/**
  * Created by Paolo on 20/02/2017.
  */
class GRNeuronalNetwork {
  private var _count = 0
  private var _lastCountEval = 0
  val list:java.util.List[DataSet] = new util.LinkedList[DataSet]()

  val numInputs = 38
  val outputNum = 2
  val iterations = 1000
  val intermediateNodes = 10
  val seed = 6
  val rate = 0.5

  val conf:MultiLayerConfiguration = new NeuralNetConfiguration.Builder()
    .seed(seed)
    .iterations(iterations)
//    .activation(Activation.TANH)
//    .weightInit(WeightInit.XAVIER)
    .learningRate(rate)
//    .regularization(true).l2(1e-4)
    .list()
    .layer(0, new DenseLayer.Builder().nIn(numInputs).nOut(intermediateNodes)
      .build())
    .layer(1, new DenseLayer.Builder().nIn(intermediateNodes).nOut(intermediateNodes)
      .build())
    .layer(2, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
      .activation(Activation.SOFTMAX)
      .nIn(intermediateNodes).nOut(outputNum).build())
    .backprop(true).pretrain(false)
    .build()

  //run the model
  val model: MultiLayerNetwork = new MultiLayerNetwork(conf)
  model.init()

  if (DEBUG.enabled) {
    model.setListeners(new ScoreIterationListener(100))
  }

  //evaluate the model on the test set
//  Evaluation eval = new Evaluation(3);
//  INDArray output = model.output(testData.getFeatureMatrix());
//  eval.eval(testData.getLabels(), output);
//  log.info(eval.stats());


  def train(example: DataSet): Unit = {
    val normalizer = new NormalizerStandardize()
//    normalizer.fit(example)
//    normalizer.transform(example)
//    println(example)

    _count += 1
    list.add(example)
  }

  def eval(example: DataSet): INDArray ={
    if (_lastCountEval != _count) {
      val nData = DataSet.merge(list)
      model.fit(nData)
      _lastCountEval = _count
    }
    val result = model.output(example.getFeatures)
    println(result)
    result
  }

  def printInfo(): Unit= {
    println(_count)
//    println(model.get)
//    println(model.getLabels)
//    model.score
  }
}
