package gestureRecognizer

import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.factory.Nd4j

/**
  * Created by Paolo on 16/02/2017.
  */

object GesturePointArray {
  val MAX_TIME = 1000
}

class GesturePointArray (val deltaMillis:Int){
  private val array = collection.mutable.ArrayBuffer.empty[GesturePoint]

  def += (x: GesturePoint): Boolean = {
    if (this.isFull) {
      return false
    }
    if (array.isEmpty) {
      array += x
      return true
    } else {
      if (x.timestamp >= array.last.timestamp + deltaMillis) {
        array += x
        return true
      }
    }
    false
  }

  def apply(i:Int): GesturePoint = array(i)

  def maxLenght:Int = GesturePointArray.MAX_TIME / deltaMillis

  def isFull: Boolean = this.maxLenght <= array.length

  def computeDifferentialArray:Array[Double] = {
    val l = maxLenght-1
    val a = Array.ofDim[Double](2, l)
    for (i <- 0 until l) {
      if ( i < array.length - 1) {
        a(0)(i) = array(i+1).squaredDistanceToGesturePoint(array(i))
        a(1)(i) = array(i+1).angleToGesturePoint(array(i))
      } else {
        a(0)(i) = 0
        a(1)(i) = 0
      }
    }
     a(0) ++ a(1)
  }

  def exportToExample(label:Int): DataSet = {
    if (label == AppStatus.TRAIN1)
      new DataSet(Nd4j.create(computeDifferentialArray), Nd4j.create(Array(1.0,0)))
    else
      new DataSet(Nd4j.create(computeDifferentialArray), Nd4j.create(Array(0,1.0)))
  }

  def differentialArrayLenght = (maxLenght - 1) * 2
}

class GesturePoint(val x:Double, val y:Double, val timestamp:Long) {
  override def toString: String = s"x: $x y: $y t=$timestamp"

  import math._

  def squaredDistanceToGesturePoint(gp: GesturePoint): Double = pow(x - gp.x, 2) + pow(y - gp.y, 2)
  def angleToGesturePoint(gp: GesturePoint): Double = atan2(gp.y, gp.x) - atan2(y, x)
}
