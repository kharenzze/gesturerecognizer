/**
  * Created by Paolo on 31/01/2017.
  */

package gestureRecognizer

import javafx.scene.input.MouseEvent

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafx.scene.canvas.Canvas
import scalafx.scene.control.Button
import scalafx.scene.paint.Color
import scalafx.scene.paint.Color._

object AppStatus {
  val TRAIN1 = 0
  val TRAIN2 = 1
  val EVAL = 2
}

object DEBUG {
  val enabled = false
}

object GestureRecognizerApp extends JFXApp {

  val defaultMillisBetweenSample = 50
  var dotArray: GesturePointArray = _
  var count = 0
  var status = 0

  var GRNN = new GRNeuronalNetwork()

  val myCanvas = new Canvas {
    height = 600
    width = 800
    onMousePressed = (event: MouseEvent) => {
      if (DEBUG.enabled) {
        println("drag entered")
      }
      dotArray = new GesturePointArray(defaultMillisBetweenSample)
    }
    onMouseDragged = (event: MouseEvent) => {
      val point = new GesturePoint(event.getSceneX, event.getSceneY, System.currentTimeMillis)
      var fillColor: Color = null
      if (dotArray != null) {
        val added = dotArray += point
        if (added) {
          if (DEBUG.enabled) {
            println(point)
          }
          fillColor = Red
        } else {
          fillColor = Black
        }
        if (!dotArray.isFull)
          this.graphicsContext2D.pixelWriter.setColor(point.x.toInt, point.y.toInt, fillColor)
      }
    }
    onMouseReleased = (event: MouseEvent) => {
      if (DEBUG.enabled) {
        println("drag exited")
      }
      cleanCanvas()
      if (status == AppStatus.EVAL) {
        GRNN.eval(dotArray.exportToExample(status))
      }
      GRNN.train(dotArray.exportToExample(status))
      count += 1
    }
  }

  val trainButton1 = new Button
  trainButton1.setText("Train 1")
  trainButton1.setMinHeight(20)
  trainButton1.setMinWidth(60)
  trainButton1.onAction = (_) => {
    status = AppStatus.TRAIN1
  }


  val trainButton2 = new Button
  trainButton2.setText("Train 2")
  trainButton2.setMinHeight(20)
  trainButton2.setMinWidth(60)
  trainButton2.layoutY = 30
  trainButton2.onAction = (_) => {
    status = AppStatus.TRAIN2
  }

  val evalButton = new Button
  evalButton.setText("Eval")
  evalButton.setMinHeight(20)
  evalButton.setMinWidth(60)
  evalButton.layoutY = 60
  evalButton.onAction = (_) => {
    status = AppStatus.EVAL
  }

  stage = new PrimaryStage {
    title = "Gesture Recognizer"
    height = 600
    width = 800
    scene = new Scene {
      fill = Grey
      content = Array(
        myCanvas,
        trainButton1,
        trainButton2,
        evalButton
      )
    }
  }

  def cleanCanvas(): Unit = {
    myCanvas.graphicsContext2D.fill = Grey
    myCanvas.graphicsContext2D.fillRect(0,0,myCanvas.width.value, myCanvas.height.value)
  }
}