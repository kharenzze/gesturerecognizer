libraryDependencies += "org.scalafx" % "scalafx_2.12" % "8.0.102-R11"
libraryDependencies += "org.deeplearning4j" % "deeplearning4j-core" % "0.7.2"
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.6.4"
// https://mvnrepository.com/artifact/org.nd4j/nd4j-native-platform
libraryDependencies += "org.nd4j" % "nd4j-native-platform" % "0.7.2"
// https://mvnrepository.com/artifact/org.datavec/datavec-api
libraryDependencies += "org.datavec" % "datavec-api" % "0.7.2"



name := "gestureRecognizer"

version := "1.0"

scalaVersion := "2.12.1"